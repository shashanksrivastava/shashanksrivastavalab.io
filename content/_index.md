## Hello World

Hi there,  
Welcome to my personal website & blog hosted on GitLab Pages.

>  **You are the architect of your life - Me**

This is one of my beliefs. Something that helps me get up when I am down.

I will be posting simple, easy to understand tutorials on coding & related stuff. Maybe sometimes I will post articles based on
my favorite topics such as Astrophysics & Biology.

This blog will remain <kbd>100% ad-free</kbd>! Forever! There are & won't be any nag screens, pop-ups, subscription requests or anything that annoys you. And thanks to it's theme, this blog looks great on mobile devices as well.
