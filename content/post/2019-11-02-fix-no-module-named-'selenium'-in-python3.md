---
title: Fix No module named 'selenium' error in Python 3.
subtitle: Learn how you can fix this error in Python3 even after installing selenium module using pip3.
date: 2019-11-02
tags: ["python3", "selenium", "module", "troubleshooting"]
---
You might have seen this error while importing selenium module in Python 3 even though you have already installed the module using `pip3 install selenium` command.

{{< highlight bash>}}
root@shashank-mbp /U/s/D/Employee-App# python3 integration-test.py
Traceback (most recent call last):
  File "integration-test.py", line 6, in <module>
    from selenium import webdriver
ModuleNotFoundError: No module named 'selenium'
{{< /highlight>}}

> And `pip` says it has already installed the `selenium` module.

{{< highlight bash>}}
root@shashank-mbp /U/shashanksrivastava# pip3 install selenium
Requirement already satisfied: selenium in /Library/Python/3.7/site-packages (3.141.0)
Requirement already satisfied: urllib3 in /Library/Python/3.7/site-packages (from selenium) (1.25.6)
{{< /highlight>}}

And the funny thing is that it works without any issues in Python 2.

So, how do we fix this problem?

After scratching my head for severel hours, I finally found that this module had to be installed manually instead of using `pip3`.

**To install the `selenium` module, simply...**

* Download the package from [selenium-3.141.0.tar.gz](https://files.pythonhosted.org/packages/ed/9c/9030520bf6ff0b4c98988448a93c04fcbd5b13cd9520074d8ed53569ccfe/selenium-3.141.0.tar.gz)

* Unpack it.
{{< highlight bash>}}
root@shashank-mbp /U/s/Downloads# tar xf selenium-3.141.0.tar.gz
root@shashank-mbp /U/s/Downloads# cd selenium-3.141.0/
root@shashank-mbp /U/s/D/selenium-3.141.0# ls
CHANGES           LICENSE           MANIFEST.in       PKG-INFO          README.rst        selenium          selenium.egg-info setup.cfg         setup.py
{{< /highlight>}}

* Now, install the module by executing `python3 setup.py install` command.
{{< highlight bash>}}
root@shashank-mbp /U/s/D/selenium-3.141.0# python3 setup.py install
running install
running bdist_egg...
Processing selenium-3.141.0-py3.7.egg
creating /usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg
Extracting selenium-3.141.0-py3.7.egg to /usr/local/lib/python3.7/site-packages
Adding selenium 3.141.0 to easy-install.pth file

Installed /usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg
Processing dependencies for selenium==3.141.0
Searching for urllib3==1.25.6
Best match: urllib3 1.25.6
Adding urllib3 1.25.6 to easy-install.pth file

Using /usr/local/lib/python3.7/site-packages
Finished processing dependencies for selenium==3.141.0
{{< /highlight>}}

Now you can import the module in Python 3.

{{< highlight bash>}}
root@shashank-mbp /U/s/D/Employee-App# python3 integration-test.py
In setUp
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56711), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
In simple
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56725), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56729), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56730), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56731), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56732), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56734), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
In tearDown
/usr/local/lib/python3.7/site-packages/selenium-3.141.0-py3.7.egg/selenium/webdriver/remote/remote_connection.py:374: ResourceWarning: unclosed <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('127.0.0.1', 56735), raddr=('127.0.0.1', 4444)>
  return self._request(command_info[0], url, body=data)
ResourceWarning: Enable tracemalloc to get the object allocation traceback
.
----------------------------------------------------------------------
Ran 1 test in 6.498s

OK
{{< /highlight>}}

**I hope you found this post useful.**

<hr>

#### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-11-02-fix-no-module-named-selenium-in-python3&t={Fix No module named 'selenium' error in Python 3}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-11-02-fix-no-module-named-selenium-in-python3&text=Fix No module named 'selenium' error in Python 3 %20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    