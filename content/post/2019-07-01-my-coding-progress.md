---
title: My Monthly Coding Progress - June
subtitle: I spent most of my time coding in June
date: 2019-07-01
tags: ["progress", "coding", "monthly-review"]
---
After starting my personal coding challenge <kbd>[Code.Every.Day](https://shashanksrivastava.gitlab.io/codeeveryday/)</kbd>, *an attempt to get better at software development*,
last month, I hadn't realized I'd stick to it religiously. There were a few days when I couldn't write any substantial
code. But I didn't give up. My motive is simple. I have to write code daily. No matter how trivial the code is. I just
have to <kbd>Code.Every.Day</kbd>.

So, this is how the month of June went for me in terms of Software Development.

![My Monthly Coding Progress - June](/img/coding-in-june.png)

I spent more than **75 hours** on **Software Development activities**! And it doesn't even include the time I spent on the
office work-station. My coding time is roughly 33% more than the next activity, i.e. Entertainment. Mind you, I am living
all alone.

Looking forward to more exciting & fruitful coding experience in July.
<hr>

#### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-07-01-my-coding-progress&t={My Monthly Coding Progress - June.}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-07-01-my-coding-progress&text=My Monthly Coding Progress - June%20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    
