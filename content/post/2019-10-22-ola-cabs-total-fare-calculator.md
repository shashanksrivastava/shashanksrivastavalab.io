---
title: Shell script to calculate the total Ola cabs fare from all the bills/invoices in a given directory.
subtitle: This Bash shell script automatically calculates the total Ola cabs fare from all the bills/invoices in a given directory. No need to open individual invoices & calculate fare manually.
date: 2019-10-22
tags: ["bash", "shell-scripting", "ola", "how-to"]
---
Have you ever felt the need to calculate the sum of all your Ola cabs bills? You open each invoice individually & calculate the fare manually, right?

I face this situation many a time & hence I decided to automate this task by writing a <kbd>shell</kbd> script. In this post, I will explain how you can
calculate the total amount of your Ola cabs bills/invoices which are stored in a particular directory.

> Please note this post assumes you have downloaded your invoices to a specific directory for better management.

Let's start now.

# Requirements.  


### 1. Python 3+ installed on your machine.

### 2. [PDFMiner](https://pypi.org/project/pdfminer/) Python module.

{{< highlight bash>}}
pip3 install pdfminer
{{< /highlight>}}

### 3. Ola bills/invoices downloaded to a particular directory.

# Instructions - How to use?

### 1. Clone the below GitHub repository.

I have created two shell scripts that will do the hard-work for you. The first script <kbd>OlaBillsDateFareExtractor.sh</kbd> extracts the date & fare from individual invoices. The second script <kbd>OlaFareTotal.sh</kbd> calculates the total amount based on the output of first one.

{{< highlight bash>}}
root@shashank-mbp /U/s/Downloads# git clone https://github.com/shashank-ssriva/ola-cabs-total-fare-calculator.git
Cloning into 'ola-cabs-total-fare-calculator'...
remote: Enumerating objects: 21, done.
remote: Counting objects: 100% (21/21), done.
remote: Compressing objects: 100% (19/19), done.
remote: Total 21 (delta 6), reused 2 (delta 0), pack-reused 0
Unpacking objects: 100% (21/21), done.
{{< /highlight>}}

You can also download the repository as a zip file by clicking this button.

<!-- GitHub repo download button. -->
<a class="github-button" href="https://github.com/shashank-ssriva/ola-cabs-total-fare-calculator/archive/master.zip" data-size="large" aria-label="Download shashank-ssriva/ola-cabs-total-fare-calculator on GitHub">Download</a>
<script async defer src="https://buttons.github.io/buttons.js"></script>

### 2. Navigate to the repository.

{{< highlight bash>}}
root@shashank-mbp /U/s/Downloads# cd ola-cabs-total-fare-calculator/
{{< /highlight>}}
<br>

### 3. Make both the shell scripts executable.

{{< highlight bash>}}
root@shashank-mbp /U/s/Downloads# chmod +x OlaBillsDateFareExtractor.sh
root@shashank-mbp /U/s/Downloads# chmod +x OlaFareTotal.sh
{{< /highlight>}}
<br>

### 4. Edit ``OlaBillsDateFareExtractor.sh`` script and enter the path (*first line*) where your Ola invoices have been downloaded.  

Here, change the directory to the one which contains your Ola invoices.
<br> 

### 5. Execute ``OlaFareTotal.sh`` script.

{{< highlight bash>}}
root@shashank-mbp /U/s/Downloads# bash OlaFareTotal.sh
The total Ola Cab fare in your bills is: 3925 Rupees (₹).
{{< /highlight>}}

Done! Now you will be able to calculate the total amount automatically, without having to open the invoices one by one.

I hope you found this post useful. I am planning to make it more intelligent in the future. But for now, it *just* works!

<hr>

### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-10-22-ola-cabs-total-fare-calculator&t={Shell script to calculate the total Ola cabs fare from all the bills/invoices in a given directory.}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-10-22-ola-cabs-total-fare-calculator&text=Shell script to calculate the total Ola cabs fare from all the bills/invoices in a given directory %20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    
