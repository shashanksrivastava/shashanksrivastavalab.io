---
title: Create a local Docker Registry on Mac.
subtitle: In this post, I will explain how you can setup a local Docker Registry on your Mac in a few easy steps.
date: 2019-11-03
tags: ["docker", "registry", "mac", "how-to"]
---
# Introduction

Docker Registry is a Docker application used to store & manage Docker images. While Docker itself has a public Registry known as 
Docker Hub which can be used to host your Docker images, chances are that you'd like to host your own images for your reference. In other words, you
don't want your custom images to be public.

In this post, I will walk you through all the steps that are needed to setup a local Docker Registry quickly on Mac. I have intentionally kept this set-up
simple by avoiding setting up reverse proxy & HTTPS. I will cover these topics later.

# Reference setup

* Host machine : - <kbd>macOS Catalina 10.15</kbd>  
* Docker Engine version : - <kbd>19.3.4</kbd>  
* Docker Compose version : - <kbd>1.24.1</kbd>

> Please note that I am not using a dedicated client here. Docker on Mac acts both as a client & a Registry. But the steps remain the same for clients as well.

# Requirements

<kbd>htpasswd</kbd> utility installed on Mac.

Apart from Docker, *obviously*, we will need `htpasswd` utility installed on Mac to generate a password for our Registry.

# Steps to follow

### Prepare data directory for Docker Registry

First, we will need to create a data directory which our Docker Registry will use to store its data.


{{< highlight bash>}}
root@shashank-mbp /U/s/# mkdir -p docker-registry/data
{{< /highlight>}}

### Create a `docker-compose.yml` file

Inside <kbd>docker-registry/data</kbd> directory, create a `docker-compose.yml` file where you will define your Docker Registry parameters like port,
data volume, etc. Using this file, Docker will pull a registry image from Docker hub.

I will explain this file later in the post.

{{< highlight yaml>}}
version: '3'

services:
  registry:
    image: registry:2
    ports:
    - "5005:5000"
    environment:
      REGISTRY_AUTH: htpasswd
      REGISTRY_AUTH_HTPASSWD_REALM: Registry
      REGISTRY_AUTH_HTPASSWD_PATH: /auth/registry.password
      REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY: /data
    volumes:
      - ./data:/data
      - ./auth:/auth
{{< /highlight>}}

### Create a directory inside docker-registry to store authentication data

Docker Registry requires a username & password to access it. So, we need to create a file using `htpasswd` command. This file will contain the credentials for our Registry.

Create a directory <kbd>auth</kbd> inside <kbd>docker-registry</kbd> & go inside it.

{{< highlight bash>}}
root@shashank-mbp /U/s/docker-registry# mkdir auth
root@shashank-mbp /U/s/docker-registry# cd auth/
{{< /highlight>}}
Now execute below command. `htpasswd` will create a file called `registry.password`. `admin` is the user which we are creating for our Registry. Feel free to choose any name.

{{< highlight bash>}}
root@shashank-mbp /U/s/d/auth# htpasswd -Bc registry.password admin
New password:
Re-type new password:
Adding password for user admin
{{< /highlight>}}

> If you take a closer look at the `docker-compose.yml` file, you will find that we are telling Docker to search for `/auth/registry.password` file for authentication. If you want to add more users to the same file, run `htpasswd` command again WITHOUT `-Bc` options. `B` encrypts the password & `c` is to create.

Let's move on to the next step.

### Run `docker-compose up` to run your Docker Registry

After executing `docker-compose up` command, you will see an output similar to below.

{{< highlight bash>}}
root@shashank-mbp /U/s/d/auth# docker-compose up
Recreating docker-registry_registry_1 ... done
Attaching to docker-registry_registry_1
{{< /highlight>}}

It indicates that our Registry is now running fine. Let's try to connect to it.

### Connect to Docker Registry

Let's login to our newly created Docker Registry. For this, open a new tab & then run <kbd>docker login localhost:5005 -u admin -p password</kbd> command. Please note that you have to use the username & password which you created using `htpasswd` command earlier.

{{< highlight bash>}}
root@shashank-mbp /U/s/docker-registry# docker login localhost:5005 -u admin -p password
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /var/root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
{{< /highlight>}}

If you receive this error while logging in...

{{< highlight bash>}}
Error saving credentials: error storing credentials - err: exit status 1, out: `error storing credentials - err: exec: "docker-credential-osxkeychain": executable file not found in $PATH, out:
{{< /highlight>}}

...then locate & remove (*or place somewhere else*) the `/usr/local/bin/docker-credential-osxkeychain` file & try again. It should fix the error.

### Now tag the Docker image that you want to publish to Registry

Once Docker Registry is running fine & you are able to log-in to it, we can push our images to it. Before we actually push, we need to tag our chosen Docker
image first. And we have to tag it using the Registry location. See below for more clarity.

{{< highlight bash>}}
root@shashank-mbp /U/s/docker-registry# docker tag employees-app localhost:5005/employees-app
{{< /highlight>}}

Here, <kbd>employees-app</kbd> is an already created Docker image which I want to push to my Registry. You can choose your own image.  

<kbd>localhost:5005/employees-app</kbd> is the tag with Registry location.

### Push the image

After tagging your image, push it to your Docker Registry. You will see an output similar to below.

{{< highlight bash>}}
root@shashank-mbp /U/s/docker-registry# docker push localhost:5005/employees-app
The push refers to repository [localhost:5005/employees-app]
dcb0d2db4aec: Pushed
27924b321589: Pushed
ba7059b20de0: Pushed
ed34be354864: Pushed
73ad47d4bc12: Pushed
{{< /highlight>}}

If you find that you are not able to push because of `internal server error 500` error, then go to the terminal tab where your `docker-compose up` command is running
and inspect the logs there.
You might see something related to file-system permissions. In my case, I was getting `filesystem: mkdir /data/docker: permission denied` error because docker daemon
didn't have permission to write to the <kbd>data</kbd> directory. Refer to step #1 where I have explained what this <kbd>data</kbd> directory does.

{{< highlight bash>}}
registry_1  | 172.18.0.1 - - [02/Nov/2019:02:47:51 +0000] "POST /v2/employees-app/blobs/uploads/ HTTP/1.1" 500 152 "" "docker/19.03.4 go/go1.12.10 git-commit/9013bf5 kernel/4.9.184-linuxkit os/linux arch/amd64 UpstreamClient(Docker-Client/19.03.4 \\(darwin\\))"
registry_1  | time="2019-11-02T02:47:51.420441Z" level=error msg="response completed with error" auth.user.name=admin err.code=unknown err.detail="filesystem: mkdir /data/docker: permission denied" err.message="unknown error" go.version=go1.11.2 http.request.host="localhost:5005" http.request.id=67431a9f-b018-4439-ad29-38c5beba130d http.request.method=POST http.request.remoteaddr="172.18.0.1:50890" http.request.uri="/v2/employees-app/blobs/uploads/" http.request.useragent="docker/19.03.4 go/go1.12.10 git-commit/9013bf5 kernel/4.9.184-linuxkit os/linux arch/amd64 UpstreamClient(Docker-Client/19.03.4 \(darwin\))" http.response.contenttype="application/json; charset=utf-8" http.response.duration=25.4095ms http.response.status=500 http.response.written=152 vars.name=employees-app
{{< /highlight>}}

To fix this, I simply had to change the permissions of `data' directory & I was then able to push my image.

### List/check the newly pushed image in your Docker Registry

Here comes the final step. To see your newly pushed image in your Docker Registry, simply execute the below command. Just make sure you are using the correct URL/port.

{{< highlight bash>}}
root@shashank-mbp /U/s/docker-registry# curl -u admin:password -H GET "http://localhost:5005/v2/_catalog"
{"repositories":["employees-app"]}
{{< /highlight>}}

Now you can pull images from your Docker Registry & create containers from them. 

Well, that's all for now. I will explain how you can setup a reverse proxy for your Docker Registry & secure it using SSL in some other post.

**I hope you found this post useful.**

<hr>

#### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-11-03-create-a-local-docker-registry-on-mac&t={Create a local Docker Registry on Mac3}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-11-03-create-a-local-docker-registry-on-mac3&text=Create a local Docker Registry on Mac %20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    