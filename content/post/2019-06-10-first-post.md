---
title: Kick off!
subtitle: Beginning my journey with Hugo
date: 2019-06-10
tags: ["blog", "hugo"]
---
  
Today I came across [Hugo](https://gohugo.io/), an awesome Static Site Generator & I eventually learned 
how to set up a blog using Hugo & GitLab.

I was looking for a writing platform which has a bold, yet clean typography. Something that offers good whitespace.
Something that makes the text stand out. Often the blogs or the web-pages are cluttered with numerous images, videos & yes, ads! It causes distraction & ruins the reading experience. I needed a change. A refreshing one.

And then came [Hugo](https://gohugo.io/). I fell in love with this [theme](https://github.com/halogenica/beautifulhugo) & immediately decided to use it for my blog. It looks so neat. So pretty. And the best part is that it's simple & never
feels overwhelming. While there are tons of blogging platforms available, what could be better than `coding` your blog
instead of *publishing* & learning cool things along?


So here it is. My new blog is ready & I am so excited about it!