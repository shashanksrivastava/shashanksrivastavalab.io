---
title: Update Sonatype Nexus on Mac
subtitle: How to update your Nexus server on Mac? 6 easy steps.
date: 2019-09-13
tags: ["nexus", "how-to", "update", "mac"]
---
In this post, I am going to show you how you can easily update Sonatype Nexus server on your Mac. By update I mean updating the minor version only, though the steps will more or less be the same for upgrades as well.

I updated from Nexus <kbd>3.15</kbd> to <kbd>3.18.1-01</kbd>, so this post is based on this version. Of course, it will be the same for any
given versions.

> Please note that downgrading to an older version is not supported & will lead to failures. Please backup the data directory before upgrading/updating.

It is important to know that the heart of any Nexus installation is its **data directory**. So, essentially we're just updating the Nexus binaries and then pointing the new version to this data directory. *More on it later*.

# Steps to perform your Nexus update.  

### 1. First, extract the new version of Nexus for Mac.

{{< highlight bash>}}
root@shashank-mbp /U/a/Downloads# tar -xf nexus-3.18.1-01-mac.tgz
root@shashank-mbp /U/a/Downloads# ls nexus-3.18.1-01/
.install4j      NOTICE.txt      OSS-LICENSE.txt PRO-LICENSE.txt bin             deploy          etc             lib             public             system
{{< /highlight>}}

### 2. Stop the Nexus server

{{< highlight bash>}}
root@shashank-mbp /U/a/Downloads# nexus stop
WARNING: ************************************************************
WARNING: Detected execution as "root" user.  This is NOT recommended!
WARNING: ************************************************************
Shutting down nexus
{{< /highlight>}}

### 3. Now, copy the <kbd>sonatype-work</kbd> directory from the old version.

As mentioned above, we have to copy the ``sonatype-work`` directory from the old Nexus installation to the new version.

> Please note that <kbd>nexus-3.18.1-01</kbd> doesn't contain ``sonatype-work`` directory by default. So, we have to copy it from the old version.


{{< highlight bash>}}
root@shashank-mbp /U/a/Downloads# cp -r nexus-3.15.0-01-mac/sonatype-work nexus-3.18.1-01/
root@shashank-mbp /U/a/Downloads# ls nexus-3.18.1-01/
.install4j      NOTICE.txt      OSS-LICENSE.txt PRO-LICENSE.txt bin             deploy          etc             lib             public          sonatype-work   system
{{< /highlight>}}

### 4. Edit the Nexus configuration file.

After copying the data directory, we now have to tell the new installation of Nexus to point to this directory. For this, we need to edit the <kbd>nexus.vmoptions</kbd> file which is located here...

{{< highlight bash>}}
/Users/admin/Downloads/nexus-3.18.1-01/bin/nexus.vmoptions
{{< /highlight>}}

Open this file & look for the lines starting with ``-Dkaraf.data`` & ``-Djava.io.tmpdir``. Then change the path accordingly. You can see below that I have specified the absolute paths. You can choose the relative paths as well.

{{< highlight bash>}}
-Dkaraf.data=/Users/admin/Downloads/nexus-3.18.1-01/sonatype-work/nexus3
-Djava.io.tmpdir=/Users/admin/Downloads/nexus-3.18.1-01/sonatype-work/nexus3/tmp
{{< /highlight>}}

### 5. Start Nexus server.

Once the paths are correctly set, we can now start our new Nexus server. But before that, it's always good to change the ``$PATH`` environment variable to use the new version. If you haven't set this variable, you can skip this part.

``$PATH`` environment variable on Mac can be set in <kbd>~/.bash_profile</kbd> file.

Now we can start our Nexus server.

{{< highlight bash>}}
root@shashank-mbp /U/a/Downloads# nexus start
WARNING: ************************************************************
WARNING: Detected execution as "root" user.  This is NOT recommended!
WARNING: ************************************************************
Starting nexus
{{< /highlight>}}

### 6. Check the new Nexus version.
  
Nexus takes a bit of time to come up. Once it is up, you can open the Nexus UI by opening your browser & entering its URL. The port can be changed in <kbd>nexus-default.properties</kbd> file which is located inside `/Users/admin/Downloads/nexus-3.18.1-01/etc` directory.

You can see below that my Nexus server has been updated.

![Nexus Server updated](/img/nexus-server-updated.png)

<hr>

### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-09-13-update-sonatype-nexus-mac&t={Update Sonatype Nexus on Mac}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-09-13-update-sonatype-nexus-mac&text=Update Sonatype Nexus on Mac %20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    
