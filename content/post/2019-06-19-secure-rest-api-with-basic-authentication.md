---
title: Enable Basic Authentication For RESTful Web-service
subtitle: Learn how to enable Basic Authentication for your RESTful web-service
date: 2019-06-19
tags: ["apache", "basic-authentication", "rest", "api"]
---
In the [last post](https://shashanksrivastava.gitlab.io/post/2019-06-18-create-rest-api-php-mysql/), I had posted about how to create a RESTful web service using PHP & MySQL.

In this post, I will show you how you can secure the same API using **Basic Authentication**. With Basic
Authentication, you'll be able to add or retrieve your To-Do's only after entering valid credentials.

Please not that these steps are based on <kbd>Apache/2.4.34</kbd> running on <kbd>macOS Mojave 10.14.5</kbd>. Depending on your OS,
your steps might vary slightly.

So let's get started.

## Steps to perform

### Create/edit `.htaccess` file.

If you have copied or cloned my [GitHub repo](https://github.com/shashank-ssriva/REST-To-Do) (*as discussed in my last post*), you
will find an `.htaccess` file there at the root of the project. 

If not, start with creating this file.

{{< highlight bash >}}
RewriteEngine On    # Turn on the rewriting engine

RewriteRule ^info/([a-zA-Z_-]*)$ info.php?task=$1 [NC,L]
AuthType Basic
AuthName "Restricted Content"
AuthUserFile /etc/apache2/.htpasswd
Require valid-user
{{< /highlight >}}

As you can see above, I am specifying the `AuthType` & a credentials file `/etc/apache2/.htpasswd`. After saving this file,
follow the next step.

### Create/edit your `/etc/apache2/.htpasswd` file.

If you haven't created this file before, create it first. Below is how to do it.

{{< highlight bash >}}
root@shashank-mbp /U/a/S/REST-To-Do# htpasswd -c /etc/apache2/.htpasswd admin
New password:
{{< /highlight >}}

Here, `admin` is the username which will be used to add/retrieve your To-Do's. Feel free to create any user. 
Now enter the password twice. It will create `/etc/apache2/.htpasswd` file for you with the credentials to
access your API.

> If this file already exists, omit the `-c` parameter in the above command. `-c` stands for **creating** the file.

### Test your API

Now try to `GET` or retrieve your To-Do info using either a browser or Postman. In case of browser, it will ask
you to enter credentials when your enter the REST endpoint in the address bar. See the image below.

![Asking for credentials]
(/img/asking-for-credentials.png)

Enter the username/password you specified while creating the `.htpasswd` file. You'll be able to access the API now.

If you're using Postman, you'll receive an error like below.

{{< highlight bash >}}
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
    <head>
        <title>401 Unauthorized</title>
    </head>
    <body>
        <h1>Unauthorized</h1>
        <p>This server could not verify that you
are authorized to access the document
requested.  Either you supplied the wrong
credentials (e.g., bad password), or your
browser doesn't understand how to supply
the credentials required.</p>
    </body>
</html>
{{< /highlight >}}

It means you need to enable authentication in Postman to access your API. For this, select **Basic Auth** from the **TYPE** dropdown
& enter your username/password. After this, click the **Send** button. You'll see your information in the bottom pane now.

Check below screenshot for more information.

![Valid Credentials]
(/img/basic-authentication.png)

<hr>

#### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-06-19-secure-rest-api-with-basic-authentication&t={Enable Basic Authentication For RESTful Web-service.}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io2019-06-19-secure-rest-api-with-basic-authentication&text=Enable%20Basic%20Authentication%20For%20RESTful%20Web-service%20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    