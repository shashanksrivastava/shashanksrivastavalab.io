---
title: Disable TLSv1 & TLSv1.1 in Java
subtitle: How to allow only TLSv1.2 in your Java applications?
date: 2019-07-14
tags: ["java", "how-to", "disable-tlsv1"]
---
As we know TLS versions 1.0 & 1.1 have been deprecated and replaced with version 1.2, it is imperative
for our applications to use TLS 1.2 by default while disabling the other two versions.

In this post, I'll show you how we can disable TLS versions 1.0 & 1.1 in our Java applications so that only
TLSv1.2 is used. This method **doesn't** require any change in the code. All we need is to modify a configuration file.

So, let's get started.

* First, locate the `java.security` file which your JDK is using. In my case, it is located here.

{{< highlight bash>}}
/Library/Java/JavaVirtualMachines/jdk1.8.0_71.jdk/Contents/Home/jre/lib/security/java.security
{{< /highlight>}}

* Now, locate the line that starts with <kbd>jdk.tls.disabledAlgorithms</kbd>.
Once you have found this line, simply add the string ``TLSv1, TLSv1.1`` to it. This should now read something like below.

{{< highlight bash>}}
jdk.tls.disabledAlgorithms=SSLv3, RC4, MD5withRSA, DH keySize < 768, TLSv1, TLSv1.1
{{< /highlight>}}

* Restart any service(s) that depend(s) on the Java process.

* Test the new settings.

Below is how you can test whether JDK is blocking the connections with TLS versions 1.0 & 1.1 or not. I am using
`openssl` command to verify. If you execute this command (**see below**), you will find that the connection gets rejected
right away. In other words, the command will immediately take you back to the command prompt when you use `tls1` or `tls1_1` as
the command argument. But in case of `tls1_2`, it will not end immediately & will wait for sometime before closing the connection.
It will also display the server certificate, it's details & a string `closed` before exiting.

Below you can see that both TLS versions 1.0 & 1.1 have been rejected and only TLSv1.2 is allowed.

### TLS version 1.0 - Rejected

{{< highlight bash>}}
root@shashank-mbp /U/admin# openssl s_client -connect localhost:8443 -tls1
CONNECTED(00000005)
4789356140:error:14004410:SSL routines:CONNECT_CR_SRVR_HELLO:sslv3 alert handshake failure:/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-22.260.1/libressl-2.6/ssl/ssl_pkt.c:1205:SSL alert number 40
4789356140:error:140040E5:SSL routines:CONNECT_CR_SRVR_HELLO:ssl handshake failure:/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-22.260.1/libressl-2.6/ssl/ssl_pkt.c:585:
---
no peer certificate available
---
No client certificate CA names sent
---
SSL handshake has read 7 bytes and written 0 bytes
---
New, (NONE), Cipher is (NONE)
Secure Renegotiation IS NOT supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1
    Cipher    : 0000
    Session-ID:
    Session-ID-ctx:
    Master-Key:
    Start Time: 1563091788
    Timeout   : 7200 (sec)
    Verify return code: 0 (ok)
---
{{< /highlight>}}

### TLS version 1.1 - Rejected

{{< highlight bash>}}
root@shashank-mbp /U/admin# openssl s_client -connect localhost:8443 -tls1_1
CONNECTED(00000005)
4582348396:error:14004410:SSL routines:CONNECT_CR_SRVR_HELLO:sslv3 alert handshake failure:/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-22.260.1/libressl-2.6/ssl/ssl_pkt.c:1205:SSL alert number 40
4582348396:error:140040E5:SSL routines:CONNECT_CR_SRVR_HELLO:ssl handshake failure:/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-22.260.1/libressl-2.6/ssl/ssl_pkt.c:585:
---
no peer certificate available
---
No client certificate CA names sent
---
SSL handshake has read 7 bytes and written 0 bytes
---
New, (NONE), Cipher is (NONE)
Secure Renegotiation IS NOT supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.1
    Cipher    : 0000
    Session-ID:
    Session-ID-ctx:
    Master-Key:
    Start Time: 1563091792
    Timeout   : 7200 (sec)
    Verify return code: 0 (ok)
---
{{< /highlight>}}

### TLS version 1.2 - Allowed

> Note that I have removed the part which contains the certificate & its details from the output.

{{< highlight bash>}}
--
No client certificate CA names sent
Server Temp Key: ECDH, P-256, 256 bits
---
SSL handshake has read 1395 bytes and written 366 bytes
---
New, TLSv1/SSLv3, Cipher is ECDHE-RSA-AES128-SHA256
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES128-SHA256
    Session-ID: 5D2AE3B6ABFCEC3FC8FF08C5F1EA7E0A995C6DBA1CBCABE66AA9EA01042FDCD2
    Session-ID-ctx:
    Master-Key: E276F444BAA61CA2AFBDE8E498EFBC5655A4C87D002CB63DE096B5527854C385DF1C6DB508AA26A31ABBE0B5D7C1EA11
    Start Time: 1563091894
    Timeout   : 7200 (sec)
    Verify return code: 18 (self signed certificate)
---
closed
{{< /highlight>}}
<hr>

#### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-07-14-disable-tlsv1-tlsv1.1-java&t={Disable TLSv1 & TLSv1.1 in Java}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-07-14-disable-tlsv1-tlsv1.1-java&text=Disable TLSv1 and TLSv1.1 in Java  %20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    