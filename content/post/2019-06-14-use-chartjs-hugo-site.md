---
title: Chart.js In Hugo
subtitle: How to use Chart.js charts in a Hugo site
date: 2019-06-14
tags: ["chart", "hugo", "chart.js"]
---

[Chart.js](https://www.chartjs.org/) is an amazing Open Source JavaScript charting library. It features beautiful, interactive charts
which can be added to web-apps, websites & of-course, *blog*!

In this post, I will explain how you can add a Chart.js chart to your Hugo site. You can see the chart live on
my blog [<kbd>Code.Every.Day</kbd>](https://shashanksrivastava.gitlab.io/codeeveryday/post/code-commit/). A live demo is available there.

## Requirements

- jQuery
- Chart.js library
- [Hugo Type theme](https://github.com/digitalcraftsman/hugo-type-theme)

This demo is based on Hugo Type theme. Steps may vary depending on the theme you are using.

## Steps to perform

### Download [jQuery](https://jquery.com/)

To minimise the network usage, I have downloaded jQuery to my local installation of Hugo. So, the first
step is to download jQuery JavaScript file to your `static/js` directory under the project directory.

*Please ensure you copy the files in the directory relative to your project directory.*

Below is the directory structure.

{{< highlight shell >}}
root@shashank-mbp /U/a/D/codeeveryday# tree static/
static/
├── css
│   └── Chart.min.css
└── js
    ├── Chart.js
    ├── Chart.min.js
    ├── chartData.js
    └── jquery-3.4.1.min.js
{{< /highlight >}}

### Download Chart.js library

Similar to jQuery, download the [Chart.js](https://www.chartjs.org/) library to `static/js` directory under the project directory. See above for the directory structure.

### Define a shortcode for the chart

Now, define a chart shortcode inside `layouts/shortcodes/chart.html` file.

You can see that I am referring to the Chart.js library here.

{{< highlight javascript >}}
<div>
    <canvas id="{{ .Get "id"}}" width="{{ .Get "width" }}" height="{{ .Get "height" }}"></canvas>
    {{ if .Get "js" }}
    <script src="../../js/Chart.min.js"></script>
    <script src="{{ .Get "js" }}"></script>
    {{ end }}
</div>
{{< /highlight >}}

### Create a new post

Create a new post in your Hugo site under `content/post/` directory.

In this post, I am invoking the **chart shortcode** which I created in the last step. You can also see here that
I am calling a JavaScript file called `chartData.js` which contains the code to display my chart. More on it in
the next step.

{{< highlight html >}}
{{%/* chart id="basicChart" js="../../js/chartData.js" */%}}
{{< /highlight >}}

### Create `chartData.js` file

As mentioned in the last step, this file will contain the code to display the chart. So, create this file
inside `static/js/` directory.

{{< highlight javascript >}}
$(function () {
  var ctx = document.getElementById('basicChart');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['10-June-19', '11-June-19', '12-June-19', '13-June-19', '14-June-19', '15-June-19'],
        datasets: [{
            label: '# of Commits',
            data: [0, 24, 94, 21, 25, 34],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
});
{{< /highlight >}}

### Run the site

Now that everything has been done, it's time to test our chart. For this, go back to your terminal & navigate to the
project directory. After this, issue below command to serve the site.

{{< highlight bash>}}
root@shashank-mbp /U/a/D/codeeveryday# hugo serve -D
{{< /highlight >}}

You can see that your chart is now visible in your post. Below is what it looks like on my Hugo site.
![Chart in Hugo]
(/img/charts-hugo.png)
<hr>

#### Share this post
<ul class="list-inline footer-links">
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://shashanksrivastava.gitlab.io/post/2019-06-14-use-chartjs-hugo-site&t={How to use Chart.js charts in a Hugo site.}" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/intent/tweet?url=https://shashanksrivastava.gitlab.io/post/2019-06-14-use-chartjs-hugo-site&text=How to use Chart.js charts in a Hugo site%20via%20@shashanksriva" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </li>
</ul>    
