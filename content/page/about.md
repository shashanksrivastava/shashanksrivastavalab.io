---
title: About me
subtitle: Who am I & What I do?
comments: true
---

My name is Shashank Srivastava. I am a self-taught DevOps Engineer with a passion for :

- Music
- Photography
- Blogging
- Reading (*I absolutely love my Kindle*)
- Coding (*for fun*)
- Travelling
- Astrophysics
- Biology, especially human physiology.

I always try to learn new things & I have started to [<kbd>Code.Every.Day</kbd>](https://shashanksrivastava.gitlab.io/codeeveryday/).

### My Blogs

- [Travel](https://shashankstraveldiary.blogspot.com/)
- [Linux/Databases/DevOps](https://watilearnd2day.wordpress.com/)
- [Book Reviews](https://thepagesbetweenfingers.blogspot.com/)
- [Music Reviews](https://mymusicrevu.blogspot.com/)
- [Fitness & How To Gain Weight](https://skinny2healthy.blogspot.in)
- [Photoblog](https://eyesmeetlenses.wordpress.com/)

### My JavaScript *(pure client-side*) Projects

- [EMI/Income Tax Calculator](https://shashankshekharsrivastava.github.io/)
- [My Music Stats](https://mymusicstats.github.io)